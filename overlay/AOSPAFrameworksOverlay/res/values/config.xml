<?xml version="1.0" encoding="utf-8"?>
<!-- Copyright (C) 2023 Paranoid Android

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
-->
<resources>

    <!-- Default list of files pinned by the Pinner Service -->
    <string-array translatable="false" name="config_defaultPinnerServiceFiles">
        <item>"/system/bin/surfaceflinger"</item>
        <item>"/system/framework/framework.jar"</item>
        <item>"/system/framework/services.jar"</item>
        <item>"/system/framework/QPerformance.jar"</item>
        <item>"/system/framework/QXPerformance.jar"</item>
        <item>"/system/framework/UxPerformance.jar"</item>
        <item>"/system/lib64/libEGL.so"</item>
        <item>"/system/lib64/libGLESv2.so"</item>
        <item>"/system/lib64/libhwui.so"</item>
        <item>"/system/system_ext/priv-app/ParanoidSystemUI/ParanoidSystemUI.apk"</item>
        <item>"/vendor/lib64/libcamxexternalformatutils.so"</item>
        <item>"/vendor/lib64/libipebpsstriping170.so"</item>
        <item>"/vendor/lib64/libipebpsstriping.so"</item>
        <item>"/vendor/lib64/egl/libEGL_adreno.so"</item>
        <item>"/vendor/lib64/egl/libGLESv2_adreno.so"</item>
        <item>"/vendor/lib64/libCB.so"</item>
        <item>"/vendor/lib64/libgsl.so"</item>
        <item>"/vendor/lib64/libllvm-glnext.so"</item>
        <item>"/vendor/lib64/libllvm-qcom.so"</item>
        <item>"/vendor/lib64/libllvm-qgl.so"</item>
        <item>"/apex/com.android.art/javalib/core-oj.jar"</item>
        <item>"/apex/com.android.art/javalib/core-libart.jar"</item>
    </string-array>

    <!-- True if home app should be pinned via Pinner Service -->
    <bool name="config_pinnerHomeApp">true</bool>

    <!-- Should the pinner service pin the Camera application? -->
    <bool name="config_pinnerCameraApp">true</bool>

    <!-- Disable stock OTA components if installed -->
    <string-array name="config_globallyDisabledComponents" translatable="false">
        <item>com.google.android.as/com.google.intelligence.sense.ambientmusic.history.AddShortcutActivity</item>
        <item>com.google.android.as/com.google.intelligence.sense.ambientmusic.history.HistoryContentProvider</item>
        <item>com.google.android.as/com.google.intelligence.sense.ambientmusic.history.HistoryActivity</item>
        <item>com.google.android.as/com.google.intelligence.sense.ambientmusic.AmbientMusicSettingsActivity</item>
        <item>com.google.android.as/com.google.intelligence.sense.ambientmusic.AmbientMusicNotificationsSettingsActivity</item>
        <item>com.google.android.as/com.google.intelligence.sense.ambientmusic.AmbientMusicSetupWizardActivity</item>
        <item>com.google.android.gms/com.google.android.gms.update.phone.PopupDialog</item>
        <item>com.google.android.gms/com.google.android.gms.update.OtaSuggestionSummaryProvider</item>
        <item>com.google.android.gms/com.google.android.gms.update.SystemUpdateActivity</item>
        <item>com.google.android.gms/com.google.android.gms.update.SystemUpdateGcmTaskService</item>
        <item>com.google.android.gms/com.google.android.gms.update.SystemUpdateService</item>
        <item>com.google.android.settings.intelligence/com.google.android.settings.intelligence.modules.batterywidget.impl.BatteryAppWidgetProvider</item>
        <item>com.google.android.settings.intelligence/com.google.android.settings.intelligence.modules.batterywidget.impl.BatteryWidgetPromoActivity</item>
    </string-array>

    <!-- Set this to true to enable the platform's auto-power-save modes like doze and
         app standby.  These are not enabled by default because they require a standard
         cloud-to-device messaging service for apps to interact correctly with the modes
         (such as to be able to deliver an instant message to the device even when it is
         dozing).  This should be enabled if you have such services and expect apps to
         correctly use them when installed on your device.  Otherwise, keep this disabled
         so that applications can still use their own mechanisms. -->
    <bool name="config_enableAutoPowerModes">true</bool>

    <!-- Whether safe headphone volume is enabled or not (country specific). -->
    <bool name="config_safe_media_volume_enabled">false</bool>

    <!--  Whether Multiuser UI should be shown -->
    <bool name="config_enableMultiUserUI">true</bool>

    <!--  Maximum number of supported users -->
    <integer name="config_multiuserMaximumUsers">8</integer>

    <!-- Colon separated list of package names that should be granted Notification Listener access -->
    <string name="config_defaultListenerAccessPackages" translatable="false">com.google.android.apps.nexuslauncher:com.google.android.setupwizard:com.google.android.apps.pixelmigrate:com.google.android.as:com.google.android.projection.gearhead:com.android.launcher3:co.aospa.glyph</string>

    <!-- Name of the font family to use in the default lockscreen clock -->
    <string name="config_clockFontFamily" translatable="false">google-sans-clock</string>

    <!-- The type of the light sensor to be used by the display framework for things like
         auto-brightness. If unset, then it just gets the default sensor of type TYPE_LIGHT. -->
    <string name="config_displayLightSensorType" translatable="false">android.sensor.light</string>

    <!-- Whether or not to enable the lock screen entry point for the QR code scanner. -->
    <bool name="config_enableQrCodeScannerOnLockScreen">true</bool>

    <!-- Default value for performant auth feature. -->
    <bool name="config_performantAuthDefault">true</bool>

    <!-- Indicate available ColorDisplayManager.COLOR_MODE_xxx. -->
    <integer-array name="config_availableColorModes">
        <item>0</item> <!-- COLOR_MODE_NATURAL -->
        <item>1</item> <!-- COLOR_MODE_BOOSTED -->
        <item>2</item> <!-- COLOR_MODE_SATURATED -->
        <item>3</item> <!-- COLOR_MODE_AUTOMATIC -->
    </integer-array>

    <!-- The following two arrays specify which color space to use for display composition when a
         certain color mode is active.
         Composition color spaces are defined in android.view.Display.COLOR_MODE_xxx, and color
         modes are defined in ColorDisplayManager.COLOR_MODE_xxx and
         ColorDisplayManager.VENDOR_COLOR_MODE_xxx.
         The color space COLOR_MODE_DEFAULT (0) lets the system select the most appropriate
         composition color space for currently displayed content. Other values (e.g.,
         COLOR_MODE_SRGB) override system selection; these other color spaces must be supported by
         the device for for display composition.
         If a color mode does not have a corresponding color space specified in this array, the
         currently set composition color space will not be modified.-->
    <integer-array name="config_displayCompositionColorModes">
        <item>0</item> <!-- COLOR_MODE_NATURAL   -->
        <item>1</item> <!-- COLOR_MODE_BOOSTED   -->
        <item>2</item> <!-- COLOR_MODE_SATURATED -->
        <item>3</item> <!-- COLOR_MODE_AUTOMATIC -->
    </integer-array>
    <integer-array name="config_displayCompositionColorSpaces">
        <item>0</item> <!-- COLOR_MODE_DEFAULT    -->
        <item>0</item> <!-- COLOR_MODE_DEFAULT    -->
        <item>0</item> <!-- COLOR_MODE_DEFAULT    -->
        <item>9</item> <!-- COLOR_MODE_DISPLAY_P3 -->
    </integer-array>

    <!-- Maximum velocity to initiate a fling, as measured in dips per second. -->
    <dimen name="config_viewMaxFlingVelocity">12000dp</dimen>

    <!-- Flag indicating whether we should enable smart battery. -->
    <bool name="config_smart_battery_available">true</bool>

    <!-- Component name for the activity that will be presenting the Recents UI, which will receive
         special permissions for API related to fetching and presenting recent tasks. The default
         configuration uses Launcehr3QuickStep as default launcher and points to the corresponding
         recents component. When using a different default launcher, change this appropriately or
         use the default systemui implementation: com.android.systemui/.recents.RecentsActivity -->
    <string name="config_recentsComponentName" translatable="false"
            >com.google.android.apps.nexuslauncher/com.android.quickstep.RecentsActivity</string>

    <!-- This is the default launcher package with an activity to use on secondary displays that
         support system decorations.
         This launcher package must have an activity that supports multiple instances and has
         corresponding launch mode set in AndroidManifest.
         {@see android.view.Display#FLAG_SHOULD_SHOW_SYSTEM_DECORATIONS} -->
    <string name="config_secondaryHomePackage" translatable="false">com.google.android.apps.nexuslauncher</string>

    <!-- Define device configs on boot -->
    <string-array name="global_device_configs_override">
 
         <!-- Adaptive Sound (Pixel 2020) -->
        <item>device_personalization_services/AdaptiveAudio__enable_adaptive_audio=true</item>
        <item>device_personalization_services/AdaptiveAudio__show_promo_notification=false</item>
        <item>device_personalization_services/AdaptiveAudio__use_silence_detector_state_bug_fix=true</item>

        <!-- Live Caption (Pixel 2017 + OEM) -->
        <item>device_personalization_services/Captions__enable_augmented_modality=true</item>
        <item>device_personalization_services/Captions__enable_augmented_modality_input=true</item>
        <item>device_personalization_services/Captions__enable_augmented_modality_language_detection=true</item>
        <item>device_personalization_services/Captions__enable_augmented_music=false</item>
        <item>device_personalization_services/Captions__enable_language_detection=true</item>
        <item>device_personalization_services/Captions__enable_punctuations=true</item>

        <!-- Machine learning back gesture (requires TensorFlow model) -->
        <item>systemui/use_back_gesture_ml_model=true</item>
        <item>systemui/back_gesture_ml_model_name=backgesture</item>
        <item>systemui/back_gesture_ml_model_threshold=0.8</item>

        <!-- Keep up to 7 days of permission usage history -->
        <item>privacy/discrete_history_cutoff_millis=604800000</item>
        <item>privacy/privacy_dashboard_7_day_toggle=true</item>

        <!-- Enable protected by android banner -->
        <item>privacy/safety_protection_enabled=true</item>

        <!-- Globally enable the new photo picker -->
        <item>storage_native_boot/take_over_get_content=true</item>

        <!-- Enable new location indicators -->
        <item>privacy/location_access_check_enabled=true</item>
        <item>privacy/location_accuracy_enabled=true</item>
        <item>privacy/location_indicator_settings_enabled=true</item>
        <item>privacy/location_indicators_enabled=true</item>
        <item>privacy/location_indicators_show_system=false</item>
        <item>privacy/location_indicators_small_enabled=true</item>

        <!-- System Ui -->
        <item>systemui/clipboard_overlay_show_actions=true</item>
        <item>systemui/quick_access_wallet_enabled=true</item>
        <item>systemui/enable_screenshot_corner_flow=true</item>
        <item>systemui/enable_screenshot_notification_smart_actions=true</item>
        <item>systemui/enable_screenshot_scrolling=true</item>
        <item>systemui/volume_separate_notification=true</item>

        <!-- Notifications -->
        <item>notification_assistant/generate_actions=true</item>
        <item>notification_assistant/generate_replies=true</item>

        <!--
            Device Personalization Services (AiAi)
        -->

        <!-- Smartspace -->
        <item>device_personalization_services/Cell__enable_cell=true</item>
        <item>device_personalization_services/Cell__enable_smartspace_events=true</item>
        <item>device_personalization_services/EchoSmartspace__check_notification_visibility=true</item>
        <item>device_personalization_services/EchoSmartspace__doorbell_when_for_update_time=true</item>
        <item>device_personalization_services/EchoSmartspace__enable_add_contextual_feedback_button_on_long_press=false</item>
        <item>device_personalization_services/EchoSmartspace__enable_add_internal_feedback_button=true</item>
        <item>device_personalization_services/EchoSmartspace__enable_agsa_settings_read=true</item>
        <item>device_personalization_services/EchoSmartspace__enable_cross_feature_rank_dedup_twiddler=true</item>
        <item>device_personalization_services/EchoSmartspace__enable_dimensional_logging=true</item>
        <item>device_personalization_services/EchoSmartspace__enable_encode_subcard_into_smartspace_target_id=true</item>
        <item>device_personalization_services/EchoSmartspace__enable_flight_landing_smartspace_aiai=true</item>
        <item>device_personalization_services/EchoSmartspace__enable_hotel_smartspace_aiai=true</item>
        <item>device_personalization_services/EchoSmartspace__enable_media_recs_for_driving=true</item>
        <item>device_personalization_services/EchoSmartspace__enable_predictor_expiration=true</item>
        <item>device_personalization_services/EchoSmartspace__enable_ring_channels_regex=true</item>
        <item>device_personalization_services/EchoSmartspace__enable_ring_using_ui_template=true</item>
        <item>device_personalization_services/EchoSmartspace__enable_travel_features_type_merge=true</item>
        <item>device_personalization_services/EchoSmartspace__ring_channels_regex=4_ding_channel_notification\d+</item>
        <item>device_personalization_services/EchoSmartspace__ring_lockscreen_delay_seconds=0</item>
        <item>device_personalization_services/EchoSmartspace__ring_on_aod_only=true</item>
        <item>device_personalization_services/EchoSmartspace__runtastic_check_pause_action=true</item>
        <item>device_personalization_services/EchoSmartspace__runtastic_is_ongoing_default_true=true</item>
        <item>device_personalization_services/EchoSmartspace__smartspace_enable_daily_forecast=true</item>
        <item>device_personalization_services/EchoSmartspace__smartspace_enable_timely_reminder=true</item>
        <item>device_personalization_services/EchoSmartspace__strava_check_stop_action=true</item>
        <item>device_personalization_services/Echo__smartspace_dedupe_fast_pair_notification=true</item>
        <item>device_personalization_services/Echo__smartspace_doorbell_aiai_loading_screen=false</item>
        <item>device_personalization_services/Echo__smartspace_doorbell_allowlist_packages=com.nest.android, com.google.android.apps.chromecast.app</item>
        <item>device_personalization_services/Echo__smartspace_doorbell_loading_screen_state=4</item>
        <item>device_personalization_services/Echo__smartspace_enable_async_icon=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_battery_notification_parser=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_bedtime_active_predictor=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_bedtime_reminder_predictor=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_bluetooth_metadata_parser=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_cross_device_timer=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_dark_launch_outlook_events=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_doorbell=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_doorbell_context_wrapper=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_doorbell_extras=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_dwb_bedtime_predictor=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_earthquake_alert_predictor=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_echo_settings=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_echo_unified_settings=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_eta_doordash=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_eta_lyft=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_food_delivery_eta=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_grocery=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_light_off_predictor=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_light_predictor=false</item>
        <item>device_personalization_services/Echo__smartspace_enable_media_wake_lock_acquire=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_nap=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_nudge=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_outlook_events=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_package_delivery=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_paired_device_connections=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_paired_device_predictor=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_ridesharing_eta=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_safety_check_predictor=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_score_ranker=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_sensitive_notification_twiddler=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_step_predictor=true</item>
        <item>device_personalization_services/Echo__smartspace_enable_subcard_logging=true</item>
        <item>device_personalization_services/Echo__smartspace_gaia_twiddler=true</item>
        <item>device_personalization_services/Echo__smartspace_outlook_event_source_of_truth=TEXT_ONLY</item>
        <item>device_personalization_services/Echo__smartspace_package_delivery_card_delay_seconds=0</item>
        <item>device_personalization_services/Echo__smartspace_show_cross_device_timer_label=true</item>
        <item>device_personalization_services/Echo_smartspace__enable_flight_landing_smartspace_aiai=true</item>
        <item>device_personalization_services/Echo_smartspace__enable_hotel_smartspace_aiai=true</item>
        <item>device_personalization_services/Echo_smartspace__smartspace_enable_daily_forecast=true</item>
        <item>device_personalization_services/Echo_smartspace__smartspace_enable_timely_reminder=true</item>

        <!-- Notification Assistant (DPS) -->
        <item>device_personalization_services/NotificationAssistant__enable_service=true</item>
        <item>device_personalization_services/NotificationAssistant__enable_upgrade_importance=true</item>
        <item>device_personalization_services/NotificationAssistant__importance_model_download_url=https://www.gstatic.com/android/notifications/importance/v1/manifest.json</item>
        <item>device_personalization_services/NotificationAssistant__importance_model_type=channel_stats</item>
        <item>device_personalization_services/NotificationAssistant__importance_model_version=1</item>
        <item>device_personalization_services/NotificationAssistant__max_importance_variance=0.5</item>

        <!-- Google Live Translate -->
        <item>device_personalization_services/Translate__app_blocklist=com.google.android.talk</item>
        <item>device_personalization_services/Translate__blue_chip_translate_enabled=true</item>
        <item>device_personalization_services/Translate__characterset_lang_detection_enabled=true</item>
        <item>device_personalization_services/Translate__copy_to_translate_enabled=true</item>
        <item>device_personalization_services/Translate__differentiate_simplified_and_traditional_chinese=true</item>
        <item>device_personalization_services/Translate__disable_session_state=false</item>
        <item>device_personalization_services/Translate__disable_translate_without_system_animation=true</item>
        <item>device_personalization_services/Translate__enable_chronicle_migration=true</item>
        <item>device_personalization_services/Translate__enable_default_langid_model=false</item>
        <item>device_personalization_services/Translate__enable_dictionary_langid_detection=true</item>
        <item>device_personalization_services/Translate__enable_language_profile_quick_update=false</item>
        <item>device_personalization_services/Translate__enable_nextdoor=false</item>
        <item>device_personalization_services/Translate__enable_opmv4_service=false</item>
        <item>device_personalization_services/Translate__enable_spa_setting=false</item>
        <item>device_personalization_services/Translate__replace_auto_translate_copied_text_enabled=true</item>
        <item>device_personalization_services/Translate__translation_service_enabled=true</item>
        <item>device_personalization_services/Translate__translator_expiration_enabled=true</item>
        <item>device_personalization_services/Translate__use_translate_kit_streaming_api=false</item>

        <!-- Gboard -->
        <item>device_personalization_services/SmartRecPixelSearch__enable_gboard_suggestion=true</item>
        <item>device_personalization_services/SmartRecPixelSearch__enable_spelling_correction=true</item>
        <item>device_personalization_services/SmartRecPixelSearch__spelling_checker_frequency_score_overrides_map={"8":-7}</item>
        <item>device_personalization_services/SmartRecCompose__enable_compose_action_filter=true</item>
        <item>device_personalization_services/SmartRecCompose__enable_deep_clu_model=true</item>
        <item>device_personalization_services/SmartDictation__enable_alternatives_from_past_corrections=true</item>
        <item>device_personalization_services/SmartDictation__enable_alternatives_from_speech_hypotheses=true</item>
        <item>device_personalization_services/SmartDictation__enable_biasing_for_commands=true</item>
        <item>device_personalization_services/SmartDictation__enable_biasing_for_contacts=true</item>
        <item>device_personalization_services/SmartDictation__enable_biasing_for_contacts_learned_from_past_corrections=true</item>
        <item>device_personalization_services/SmartDictation__enable_biasing_for_interests_model=true</item>
        <item>device_personalization_services/SmartDictation__enable_biasing_for_past_correction=true</item>
        <item>device_personalization_services/SmartDictation__enable_biasing_for_screen_context=true</item>
        <item>device_personalization_services/SmartDictation__enable_selection_filtering=true</item>

        <!-- Pixel Launcher -->
        <item>launcher/enable_one_search=true</item>
        <item>launcher/ENABLE_LOCAL_RECOMMENDED_WIDGETS_FILTER=true</item>
        <item>launcher/ENABLE_SMARTSPACE_ENHANCED=true</item>
        <item>launcher/ENABLE_WIDGETS_PICKER_AIAI_SEARCH=true</item>
        <item>launcher/ENABLE_QUICK_LAUNCH_V2=true</item>
        <item>launcher/GBOARD_UPDATE_ENTER_KEY=true</item>
        <item>launcher/enable_quick_launch_v2=true</item>
     
        <!-- Pixel Launcher (Recents) -->
        <item>device_personalization_services/Overview__enable_barcode_detection=false</item>
        <item>device_personalization_services/Overview__enable_image_search=false</item>
        <item>device_personalization_services/Overview__enable_lens_r_overview_long_press=true</item>
        <item>device_personalization_services/Overview__enable_lens_r_overview_select_mode=true</item>
        <item>device_personalization_services/Overview__enable_lens_r_overview_translate_action=true</item>
        <item>device_personalization_services/Overview__enable_image_selection=true</item>
        <item>device_personalization_services/Overview__enable_japanese_ocr=true</item>
        <item>device_personalization_services/Overview__enable_overview=true</item>
        <item>device_personalization_services/Overview__enable_pir_clearcut_logging=true</item>
        <item>device_personalization_services/Overview__enable_pir_westworld_logging=true</item>
        <item>device_personalization_services/Overview__enable_proactive_hints=false</item>
        <item>device_personalization_services/Overview__enable_superpacks_pir_protocol=true</item>
        <item>device_personalization_services/Overview__min_lens_agsa_app_version=301084649</item>
    </string-array>

</resources>
